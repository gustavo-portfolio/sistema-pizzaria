/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.controller;
import pizzaria.view.CadastrarPedido;

import pizzaria.model.Pedido;
import pizzaria.model.SaborPizza;
import pizzaria.model.Pizza;

import pizzaria.model.DAO.PedidoDAO;
import pizzaria.model.DAO.SaborPizzaDAO;
import pizzaria.model.DAO.ClienteDAO;
import pizzaria.model.DAO.PizzaDAO;

import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

/**
 *
 * @author slaif
 */
public class PedidoController 
{
    private CadastrarPedido view;
    private PedidoDAO dao;
    
    public PedidoController(CadastrarPedido view, PedidoDAO dao)
    {
        this.view = view;
        this.dao = dao;
        initController();
    }
    
    public void initController()
    {
        this.view.setController(this);
        this.view.initView();
        listarSabores();
    }
    
    public void listarSabores()
    {
        try
        {
            SaborPizzaDAO daoSabor = new SaborPizzaDAO();
            this.view.listarSabores(daoSabor.listar());
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao listar os sabores!", "Erro");
        }
    }
    
    public void buscarPedidosCliente()
    {
        try
        {
            ClienteDAO daoCliente = new ClienteDAO();
            
            this.view.limparTabelaCliente();
            this.view.limparTabelaPizza();
            
            this.view.novoPedido(daoCliente.buscarTelefone(this.view.getSelectedTelefone()));
            this.view.listarPedidosCliente(this.dao.listarPorTelefone(this.view.getSelectedTelefone()));
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao buscar o pedido do cliente!", "Erro");
        }
    }
    
    public void buscarPizzasPedido()
    {
        try
        {
            this.view.limparTabelaPizza();
            
            if(this.view.getSelectedPedido().getId() > 0)
            {
                PizzaDAO dao = new PizzaDAO();
                this.view.listarPizzasPedido(dao.listarPorPedido(this.view.getSelectedPedido().getId()));
            }
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao buscar as pizzas do pedido!", "Erro");
        }
    }
    
    private int adicionarPedido()
    {
        Pedido novoPedido = new Pedido();
        
        novoPedido.getCliente().setId(this.view.getSelectedPedido().getCliente().getId());
        novoPedido.getEstado().setEstado("Aberto");
        novoPedido.setPrecoTotal(0);
        
        this.dao.inserir(novoPedido);
        
        return novoPedido.getId();
    }
    
    public void adicionarPizza()
    {
        try
        {
            PizzaDAO daoPizza = new PizzaDAO();
            SaborPizzaDAO daoSabor = new SaborPizzaDAO();
            
            Pizza novaPizza = this.view.getNewPizza();
            
            if(novaPizza.getForma().getLado() > 0)
            {
                int idPedido = this.view.getSelectedPedido().getId();
                
                if(idPedido == 0)
                    idPedido = adicionarPedido();
                
                novaPizza.getSabores()[0].getTipo().setPrecoCM2(daoSabor.buscarPrecoSabor(novaPizza.getSabores()[0].getSabor()));
                novaPizza.getSabores()[1].getTipo().setPrecoCM2(daoSabor.buscarPrecoSabor(novaPizza.getSabores()[1].getSabor()));
                novaPizza.getPreco();

                daoPizza.inserir(novaPizza);

                this.dao.inserirPizza(idPedido, novaPizza.getId());
                double valorTotal = this.dao.buscarValorTotalPedido(idPedido);
                this.dao.atualizarValorTotalPedido(idPedido, valorTotal);

                this.view.limparInputs();
                this.view.adicionarPizza(novaPizza);
                this.view.preencherPrecoTotal(valorTotal);
                this.view.preencherIdPedido(idPedido);
            }
            else
                this.view.errorMessage("O comprimento deve ser maior!", "Erro");
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao adicionar a pizza ao pedido!", "Erro");
        }
    }
    
    public void alterarPizza()
    {
        try
        {
            PizzaDAO daoPizza = new PizzaDAO();
            SaborPizzaDAO daoSabor = new SaborPizzaDAO();
            
            Pizza pizzaAtualizada = this.view.getUpdatedPizza();
            
            if(pizzaAtualizada.getForma().getLado() > 0)
            {
                pizzaAtualizada.getSabores()[0].getTipo().setPrecoCM2(daoSabor.buscarPrecoSabor(pizzaAtualizada.getSabores()[0].getSabor()));
                pizzaAtualizada.getSabores()[1].getTipo().setPrecoCM2(daoSabor.buscarPrecoSabor(pizzaAtualizada.getSabores()[1].getSabor()));
                pizzaAtualizada.getPreco();
                
                daoPizza.alterar(pizzaAtualizada);
                
                double valorTotal = this.dao.buscarValorTotalPedido(this.view.getSelectedPedido().getId());
                this.dao.atualizarValorTotalPedido(this.view.getSelectedPedido().getId(), valorTotal);

                this.view.limparInputs();
                this.view.alterarPizza(pizzaAtualizada);
                this.view.preencherPrecoTotal(valorTotal);
            }
            else
                this.view.errorMessage("O comprimento deve ser maior!", "Erro");
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao alterar a pizza do pedido!", "Erro");
        }
    }
    
    public void calcularComprimento()
    {
        try
        {
            double area = Double.parseDouble(this.view.getSelectedArea());
            
            if(area >= 100 && area <= 1600)
            {
                double comprimento;
                
                if(this.view.getSelectedForma().equalsIgnoreCase("Quadrado"))
                    comprimento = Math.sqrt(area);
                else if(this.view.getSelectedForma().equalsIgnoreCase("Circulo"))
                    comprimento = Math.sqrt(area / Math.PI);
                else // Triângulo
                    comprimento = Math.sqrt((4 * area) / Math.sqrt(3));
                
                this.view.limparInputs();
                this.view.preencherComprimento(comprimento);
            }
            else
                this.view.errorMessage("A área deve estar entre 100 e 1600!", "Erro");
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao calcular o comprimento!", "Erro");
        }
    }
}
