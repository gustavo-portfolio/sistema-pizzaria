/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.controller;
import pizzaria.view.*;
import pizzaria.model.DAO.*;

/**
 *
 * @author slaif
 */
public class HeaderController 
{
    private CadastrarCliente viewCliente;
    private CadastrarPedido viewPedido;
    private CadastrarSabor viewSabor;
    private PrecoCM2 viewPreco;
    private VisualizarEstadoPedido viewEstado;
    
    public HeaderController()
    {
        viewCliente = new CadastrarCliente();
        viewPedido = new CadastrarPedido();
        viewSabor = new CadastrarSabor();
        viewPreco = new PrecoCM2();
        viewEstado = new VisualizarEstadoPedido();
    }
    
    public void initTelaCliente(javax.swing.JFrame tela)
    {
        tela.dispose();
        
        ClienteDAO daoCliente = new ClienteDAO();
        ClienteController clienteController = new ClienteController(this.viewCliente, daoCliente);
    }
    
    public void initTelaPedido(javax.swing.JFrame tela)
    {
        tela.dispose();
        
        PedidoDAO daoPedido = new PedidoDAO();
        PedidoController pedidoController = new PedidoController(viewPedido, daoPedido);
    }
    
    public void initTelaSabor(javax.swing.JFrame tela)
    {
        tela.dispose();
        
        SaborPizzaDAO daoSabor = new SaborPizzaDAO();
        SaborController saborController = new SaborController(this.viewSabor, daoSabor);
    }
    
    public void initTelaPreco(javax.swing.JFrame tela)
    {
        tela.dispose();
        
        TipoPizzaDAO daoTipo = new TipoPizzaDAO();
        PrecoController precoController = new PrecoController(this.viewPreco, daoTipo);
    }
    
    public void initTelaEstadoPedido(javax.swing.JFrame tela)
    {
        tela.dispose();
        
        PedidoDAO daoPedido = new PedidoDAO();
        EstadoController estadoController = new EstadoController(this.viewEstado, daoPedido);
    }
}
