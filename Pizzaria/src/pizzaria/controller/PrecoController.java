/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.controller;

import pizzaria.model.TipoPizza;
import pizzaria.model.DAO.TipoPizzaDAO;
import pizzaria.view.PrecoCM2;

/**
 *
 * @author slaif
 */
public class PrecoController 
{
    private PrecoCM2 view;
    private TipoPizzaDAO dao;
    
    public PrecoController(PrecoCM2 view, TipoPizzaDAO dao)
    {
        this.view = view;
        this.dao = dao;
        initController();
    }
    
    public void initController()
    {
        this.view.setController(this);
        this.view.initView();
        this.view.listarTabela(this.dao.listar());
    }
    
    public void atualizarPreco()
    {
        try
        {
            TipoPizza tipoAtualizado = this.view.getSelectedTipo();
            
            if(tipoAtualizado.getPrecoCM2() > 0)
            {
                this.dao.atualizarPreco(tipoAtualizado);
                
                this.view.limparTabela();
                this.view.listarTabela(dao.listar());
            }
            else
                this.view.errorMessage("O preço deve ser positivo!", "Erro");
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao atualizar o preço!", "Erro");
        }
    }
}
