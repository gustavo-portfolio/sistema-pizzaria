/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.controller;
import pizzaria.model.DAO.*;
import pizzaria.view.*;

/**
 *
 * @author slaif
 */
public class Main {
    public static void main(String args[])
    {
        CadastrarCliente viewCliente = new CadastrarCliente();
        ClienteDAO daoCliente = new ClienteDAO();
        ClienteController clienteController = new ClienteController(viewCliente, daoCliente);
    }
}
