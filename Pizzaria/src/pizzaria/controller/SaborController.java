/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.controller;
import pizzaria.model.SaborPizza;
import pizzaria.model.DAO.SaborPizzaDAO;
import pizzaria.view.CadastrarSabor;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author slaif
 */
public class SaborController 
{
    private CadastrarSabor view;
    private SaborPizzaDAO dao;
    
    public SaborController(CadastrarSabor view, SaborPizzaDAO dao)
    {
        this.view = view;
        this.dao = dao;
        initController();
    }
    
    public void initController()
    {
        this.view.setController(this);
        this.view.initView();
    }
    
    public void listarSabores()
    {
        try
        {
            this.view.limparTabela();
            this.view.listarTabela(dao.listar());
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao listar sabores!", "Erro");
        }
    }
    
    public void adicionarSabor()
    {
        try
        {
            SaborPizza novoSabor = this.view.getSelectedSabor();
            
            if(novoSabor.getSabor().isBlank())
                this.view.errorMessage("Preencha o campo 'Sabor'!", "Erro");
            else
            {
                this.dao.inserir(novoSabor);
                this.view.inserirTabela(novoSabor);
            }
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao adicionar o sabor!", "Erro");
        }
    }
}
