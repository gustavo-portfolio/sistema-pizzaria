/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.controller;
import pizzaria.view.VisualizarEstadoPedido;
import pizzaria.model.Pedido;
import pizzaria.model.DAO.PedidoDAO;

/**
 *
 * @author slaif
 */
public class EstadoController 
{
    private VisualizarEstadoPedido view;
    private PedidoDAO dao;
    
    public EstadoController(VisualizarEstadoPedido view, PedidoDAO dao)
    {
        this.view = view;
        this.dao = dao;
        initController();
    }
    
    public void initController()
    {
        this.view.setController(this);
        this.view.initView();
        this.view.listarTabela(this.dao.listar());
    }
    
    public void listarPedidos()
    {
        try
        {
            this.view.limparTabela();
            this.view.listarTabela(this.dao.listar());
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao listar os pedidos!", "Erro");
        }
    }
    
    public void alterarEstado()
    {
        try
        {
            this.dao.alterarEstado(this.view.getSelectedPedido());
            this.view.limparTabela();
            this.view.listarTabela(this.dao.listar());
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao alterar o estado do pedido!", "Erro");
        }
    }
}
