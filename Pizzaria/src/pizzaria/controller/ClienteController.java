/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.controller;
import pizzaria.view.CadastrarCliente;
import pizzaria.model.Cliente;
import pizzaria.model.DAO.ClienteDAO;

/**
 *
 * @author slaif
 */
public class ClienteController 
{
    private CadastrarCliente view;
    private ClienteDAO dao;
    
    public ClienteController(CadastrarCliente view, ClienteDAO dao)
    {
        this.view = view;
        this.dao = dao;
        initController();
    }
    
    public void initController()
    {
        this.view.setController(this);
        this.view.initView();
    }
    
    public void listarClientes()
    {
        try
        {
            this.view.limparTabela();
            this.view.listarTabela(this.dao.listar());
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao listar os clientes!", "Erro");
        }
    }
    
    public void adicionarCliente()
    {
        try
        {
            Cliente cliente = this.view.getNewCliente();
            
            if(cliente.getNome().isBlank() || cliente.getSobreNome().isBlank() || cliente.getTelefone().isBlank())
                this.view.errorMessage("Preencha os campos!", "Erro");
            else
            {
                this.dao.inserir(cliente);
                this.view.limparTabela();
                this.view.listarTabela(this.dao.listar());
                this.view.limparInput();
            }
            
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao inserir cliente!", "Erro");
        }
    }
    
    public void removerCliente()
    {
        try
        {
            this.dao.remover(this.view.getSelectedCliente());
            this.view.limparTabela();
            this.view.listarTabela(this.dao.listar());
            this.view.limparInput();
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao remover cliente!", "Erro");
        }
    }
    
    public void alterarCliente()
    {
        try
        {
            Cliente cliente = this.view.getNewCliente();
            
            if(cliente.getNome().isBlank() || cliente.getSobreNome().isBlank() || cliente.getTelefone().isBlank())
                this.view.errorMessage("Preencha os campos!", "Erro");
            else
            {
                this.dao.alterar(this.view.getSelectedCliente());
                this.view.limparTabela();
                this.view.listarTabela(this.dao.listar());
                this.view.limparInput();
            } 
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao alterar cliente!", "Erro");
        }
    }
    
    public void buscarClienteSobrenome()
    {
        try
        {
            Cliente cliente = this.dao.buscarSobrenome(this.view.getSelectedSobrenome());
            this.view.limparTabela();
            this.view.listarTabela(cliente);
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao buscar Cliente!", "Erro");
        }
    }
    
    public void buscarClienteTelefone()
    {
        try
        {
            Cliente cliente = this.dao.buscarTelefone(this.view.getSelectedTelefone());
            this.view.limparTabela();
            this.view.listarTabela(cliente);
        }
        catch (Exception e)
        {
            this.view.errorMessage("Erro ao buscar Cliente!", "Erro");
        }
    }
}
