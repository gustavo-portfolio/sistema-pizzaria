/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

/**
 *
 * @author slaif
 */
public class TipoPizza 
{
    private String tipo;
    private double precoCM2;
    
    public TipoPizza(){}
    
    public TipoPizza(String tipo, double precoCM2)
    {
        this.tipo = tipo;
        this.precoCM2 = precoCM2;
    }
    
    /* Getters */
    public String getNomeTipo()
    {
        return this.tipo;
    }
    
    public double getPrecoCM2()
    {
        return this.precoCM2;
    }
    
    /* Setters */
    public void setNomeTipo(String tipo)
    {
        this.tipo = tipo;
    }
    
    public void setPrecoCM2(double preco)
    {
        this.precoCM2 = preco;
    }
}
