/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;
import java.lang.Math;

/**
 *
 * @author slaif
 */
public class Triangulo extends Forma
{
    private double lado;
    private final String nomeForma = "Triangulo";
    
    public Triangulo(){}
    
    @Override
    public String getNomeForma()
    {
        return this.nomeForma;
    }
    
    @Override
    public boolean setTamanho(double tamanho)
    {
        if(tamanho >= 20 && tamanho <= 60)
        {
            this.lado = tamanho;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public double calcularArea()
    {
        return (Math.pow(this.lado, 2) * Math.sqrt(3)) / 4;
    }
    
    @Override
    public double getLado()
    {
        return this.lado;
    }
}
