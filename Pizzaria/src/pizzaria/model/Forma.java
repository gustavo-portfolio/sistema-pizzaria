/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

/**
 *
 * @author slaif
 */
public abstract class Forma 
{
    public abstract String getNomeForma();
    public abstract double getLado();
    public abstract boolean setTamanho(double tamanho);
    public abstract double calcularArea();
}
