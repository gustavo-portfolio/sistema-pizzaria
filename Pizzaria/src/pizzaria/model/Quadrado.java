/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

/**
 *
 * @author slaif
 */
public class Quadrado extends Forma
{
    private double lado;
    private final String nomeForma = "Quadrado";
    
    public Quadrado(){}
    
    @Override
    public String getNomeForma()
    {
        return this.nomeForma;
    }
        
    @Override
    public boolean setTamanho(double tamanho)
    {
        if(tamanho >= 10 && tamanho <= 40)
        {
            this.lado = tamanho;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public double calcularArea()
    {
        return this.lado * this.lado;
    }

    @Override
    public double getLado()
    {
        return this.lado;
    }
}
