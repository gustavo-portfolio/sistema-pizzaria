/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model.DAO;
import pizzaria.model.SaborPizza;
import pizzaria.model.TipoPizza;

import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author slaif
 */
public class SaborPizzaDAO 
{
    private final String listar = "SELECT * FROM SaborPizza";
    private final String inserir = "INSERT INTO SaborPizza(sabor, FK_tipo) VALUES (?, ?)";
    private final String buscarSabor = "SELECT * FROM SaborPizza WHERE sabor=?";
    private final String buscarSaboresPizza = "SELECT FK_sabor FROM SaboresPizza WHERE FK_idPizza=?";
    private final String buscarPrecoSabor = "SELECT * FROM SaborPizza INNER JOIN TipoPizza ON FK_tipo = tipo WHERE sabor=?";
    
    public List<SaborPizza> listar()
    {
        List<SaborPizza> lista = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listar);
            ResultSet resultado = stmt.executeQuery();)
        {
            while(resultado.next())
            {
                SaborPizza sabor = new SaborPizza(); 
                TipoPizza tipo = new TipoPizza();
                
                sabor.setSabor(resultado.getString("sabor"));
                tipo.setNomeTipo(resultado.getString("FK_tipo"));
                sabor.setTipo(tipo);
                
                lista.add(sabor);
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar dados no banco de dados. Origem="+e.getMessage());
        }
        
        return lista;
    }
    
    public void inserir(SaborPizza sabor)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.inserir);)
        {
            stmt.setString(1, sabor.getSabor());
            stmt.setString(2, sabor.getTipo().getNomeTipo());
            
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao inserir SaborPizza no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public SaborPizza buscarSabor(String sabor)
    {
        SaborPizza buscaSabor = new SaborPizza();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.buscarSabor);)
        {
            stmt.setString(1, sabor);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                if(resultado.next())
                {
                    buscaSabor.setSabor(resultado.getString("sabor"));
                    buscaSabor.getTipo().setNomeTipo(resultado.getString("FK_tipo"));
                }
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao buscar sabor no banco de dados. Origem="+e.getMessage());
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar sabor no banco de dados. Origem="+e.getMessage());
        }
        
        return buscaSabor;
    }
    
    public List<String> buscarSaboresPizza(int idPizza)
    {
        List<String> sabores = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.buscarSaboresPizza);)
        {
            stmt.setInt(1, idPizza);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                while(resultado.next())
                    sabores.add(resultado.getString("FK_sabor"));
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao buscar sabores da pizza no banco de dados. Origem="+e.getMessage());
            }
            
            return sabores;
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar sabores da pizza no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public double buscarPrecoSabor(String sabor)
    {
        double preco = 0;
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.buscarPrecoSabor);)
        {
            stmt.setString(1, sabor);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                if(resultado.next())
                    preco = resultado.getDouble("precoCM2");
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao buscar preço do sabor no banco de dados. Origem="+e.getMessage());
            }
            
            return preco;
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar preço do sabor no banco de dados. Origem="+e.getMessage());
        }
    }
}
