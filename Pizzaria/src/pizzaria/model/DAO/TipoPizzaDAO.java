/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model.DAO;
import pizzaria.model.TipoPizza;

import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author slaif
 */
public class TipoPizzaDAO 
{
    public final String listar = "SELECT * FROM TipoPizza";
    public final String atualizarPreco = "UPDATE TipoPizza SET precoCM2=? WHERE tipo=?";
    
    public List<TipoPizza> listar()
    {
        List<TipoPizza> lista = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listar);
            ResultSet resultado = stmt.executeQuery();)
        {
            while(resultado.next())
            {
                TipoPizza tipo = new TipoPizza();
                
                tipo.setNomeTipo(resultado.getString("tipo"));
                tipo.setPrecoCM2(resultado.getDouble("precoCM2"));
                
                lista.add(tipo);
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar dados no banco de dados. Origem="+e.getMessage());
        }
        
        return lista;
    }
    
    public void atualizarPreco(TipoPizza tipo)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.atualizarPreco);)
        {
            stmt.setDouble(1, tipo.getPrecoCM2());
            stmt.setString(2, tipo.getNomeTipo());
            
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao alterar TipoPizza no banco de dados. Origem="+e.getMessage());
        }
    }
}
