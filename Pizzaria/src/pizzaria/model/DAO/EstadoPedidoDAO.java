/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model.DAO;
import pizzaria.model.EstadoPedido;

import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author slaif
 */
public class EstadoPedidoDAO {
    private final String listar = "SELECT * FROM EstadoPedido";
    
    public List<EstadoPedido> listar()
    {
        //Connection con = null;
        //PreparedStatement stmt = null;
        //ResultSet resultado = null;
        List<EstadoPedido> listaEstados = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listar);
            ResultSet resultado = stmt.executeQuery();)
        {
            while(resultado.next())
            {
                EstadoPedido estado = new EstadoPedido();
                
                estado.setEstado(resultado.getString("estado"));
                listaEstados.add(estado);
            }
        }
        catch(SQLException e)
        {
            throw new RuntimeException("Erro ao buscar dados no banco de dados. Origem="+e.getMessage());
        }
        
        return listaEstados;
    }
}
