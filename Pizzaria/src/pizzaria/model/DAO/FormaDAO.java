/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model.DAO;

import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author slaif
 */
public class FormaDAO 
{
    private final String listar = "SELECT * FROM FormaPizza";
    
    public List<String> listar()
    {
        List<String> lista = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listar);
            ResultSet resultado = stmt.executeQuery();)
        {
            while(resultado.next())
            {
                lista.add(resultado.getString("forma"));
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar dados no banco de dados. Origem="+e.getMessage());
        }
        
        return lista;
    }
}
