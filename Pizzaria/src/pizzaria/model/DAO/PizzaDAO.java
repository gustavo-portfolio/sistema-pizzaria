/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model.DAO;
import pizzaria.model.Pizza;
import pizzaria.model.SaborPizza;

import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import pizzaria.model.TipoPizza;

/**
 *
 * @author slaif
 */
public class PizzaDAO 
{
    private final String inserir = "INSERT INTO Pizza(FK_forma, lado, preco) VALUES (?, ?, ?)";
    private final String inserirSaboresPizza = "INSERT INTO SaboresPizza(FK_idPizza, FK_sabor) VALUES (?, ?)";
    
    private final String alterar = "UPDATE Pizza SET FK_forma=?, lado=?, preco=? WHERE idPizza=?";
    private final String alterarSaboresPizza = "UPDATE SaboresPizza SET FK_sabor=? WHERE FK_idPizza=? AND FK_sabor=?";
    
    private final String listarPorPedido = "SELECT * FROM PizzasPedido INNER JOIN Pizza ON FK_idPizza = idPizza WHERE FK_idPedido=?";
    
    private final String remover = "DELETE FROM SaboresPizza WHERE FK_idPizza=?";
    
    public void inserir(Pizza pizza)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.inserir, PreparedStatement.RETURN_GENERATED_KEYS);)
        {
            con.setAutoCommit(false);
            
            stmt.setString(1, pizza.getForma().getNomeForma());
            stmt.setDouble(2, pizza.getForma().getLado());
            stmt.setDouble(3, pizza.getPreco());
            
            stmt.executeUpdate();
            
            ResultSet resultado = stmt.getGeneratedKeys();
            resultado.next();
            
            pizza.setId(resultado.getInt(1));
            int qtdSabores = 2;
            
            if(pizza.getSabores()[0].getSabor().equalsIgnoreCase(pizza.getSabores()[1].getSabor()))
                qtdSabores--;
            
            // Inserir na relação n-n
            try(PreparedStatement stmt2 = con.prepareStatement(this.inserirSaboresPizza);)
            {
                for(int i = 0; i < qtdSabores; i++)
                {
                    stmt2.setInt(1, pizza.getId());
                    stmt2.setString(2, pizza.getSabores()[i].getSabor());
                    
                    stmt2.executeUpdate();
                }
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao inserir SaboresPizza no banco de dados. Origem="+e.getMessage());
            }
            
            con.commit();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao inserir pizza no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public void alterar(Pizza pizza)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.alterar);)
        {
            con.setAutoCommit(false);
            
            stmt.setString(1, pizza.getForma().getNomeForma());
            stmt.setDouble(2, pizza.getForma().getLado());
            stmt.setDouble(3, pizza.getPreco());
            stmt.setInt(4, pizza.getId());
            
            stmt.executeUpdate();
            
            removerSabores(pizza);
            
            // Inserir na relação n-n
            int qtdSabores = 2;
            
            if(pizza.getSabores()[0].getSabor().equalsIgnoreCase(pizza.getSabores()[1].getSabor()))
                qtdSabores--;
            
            try(PreparedStatement stmt2 = con.prepareStatement(this.inserirSaboresPizza);)
            {
                for(int i = 0; i < qtdSabores; i++)
                {
                    stmt2.setInt(1, pizza.getId());
                    stmt2.setString(2, pizza.getSabores()[i].getSabor());
                    
                    stmt2.executeUpdate();
                }
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao inserir SaboresPizza no banco de dados. Origem="+e.getMessage());
            }
            
            con.commit();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao atualizar Pizza no banco de dados. Origem="+e.getMessage());
        }
    }
    
    private List<String> buscarSabores(Pizza pizza)
    {
        String sql = "SELECT FK_sabor FROM SaboresPizza WHERE FK_idPizza=?";
        List<String> sabores = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);)
        {
            stmt.setInt(1, pizza.getId());
            
            try(ResultSet resultado = stmt.executeQuery())
            {
                while(resultado.next())
                    sabores.add(resultado.getString("FK_sabor"));
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao buscar sabores no banco de dados. Origem="+e.getMessage());
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar sabores no banco de dados. Origem="+e.getMessage());
        }
        
        return sabores;
    }
    
    public List<Pizza> listarPorPedido(int idPedido)
    {
        List<Pizza> lista = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listarPorPedido))
        {
            stmt.setInt(1, idPedido);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                while(resultado.next())
                {
                    Pizza pizza = new Pizza();
                    SaborPizzaDAO dao = new SaborPizzaDAO();
                    List<String> sabores = dao.buscarSaboresPizza(resultado.getInt("idPizza"));
                    
                    pizza.setId(resultado.getInt("idPizza"));
                    pizza.setPreco(resultado.getDouble("preco"));
                    pizza.setForma(resultado.getString("FK_forma"));
                    pizza.getForma().setTamanho(resultado.getDouble("lado"));
                    
                    for(int i = 0; i < sabores.size(); i++)
                    {
                        pizza.getSabores()[i] = new SaborPizza(sabores.get(i), new TipoPizza());
                        pizza.getSabores()[i].getTipo().setPrecoCM2(dao.buscarPrecoSabor(sabores.get(i)));
                    } 
                    
                    if(sabores.size() == 1)
                        pizza.getSabores()[1] = pizza.getSabores()[0];
                    
                    lista.add(pizza);
                }
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao buscar pizzas do pedido no banco de dados. Origem="+e.getMessage());
            }
            
            return lista;
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar pizzas do pedido no banco de dados. Origem="+e.getMessage());
        }
    }
    
    private void removerSabores(Pizza pizza)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.remover);)
        {
            stmt.setInt(1, pizza.getId());
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao remover pizzas do pedido no banco de dados. Origem="+e.getMessage());
        }
    }
}
