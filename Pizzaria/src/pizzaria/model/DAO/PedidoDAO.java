/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model.DAO;
import pizzaria.model.Pedido;
import pizzaria.model.EstadoPedido;
import pizzaria.model.Cliente;
import pizzaria.model.Pizza;

import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author slaif
 */
public class PedidoDAO 
{
    private final String listar = "SELECT * FROM Pedido INNER JOIN Cliente ON FK_idCliente = idCliente";
    private final String listarPorTelefone = "SELECT * FROM Pedido INNER JOIN Cliente ON FK_idCliente = idCliente WHERE telefone=?";
    private final String alterarEstado = "UPDATE Pedido SET FK_estado=? WHERE idPedido=?";
    private final String inserir = "INSERT INTO Pedido(FK_idCliente, FK_estado, precoTotal) VALUES (?, ?, ?)";
    private final String inserirPizza = "INSERT INTO PizzasPedido(FK_idPedido, FK_idPizza) VALUES (?, ?)";
    private final String buscarValorTotalPedido = "SELECT SUM(preco) AS precoTotal FROM PizzasPedido INNER JOIN Pizza ON FK_idPizza = idPizza WHERE FK_idPedido=?";
    private final String atualizarValorTotalPedido = "UPDATE Pedido SET precoTotal=? WHERE idPedido=?";
    
    public List<Pedido> listar()
    {
        List<Pedido> lista = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listar);
            ResultSet resultado = stmt.executeQuery();)
        {
            while(resultado.next())
            {
                Pedido pedido = new Pedido();

                pedido.setId(resultado.getInt("idPedido"));
                pedido.setCliente(new Cliente(resultado.getInt("FK_idCliente"), resultado.getString("nome"), resultado.getString("sobrenome"), resultado.getString("telefone")));
                pedido.setEstado(new EstadoPedido(resultado.getString("FK_estado")));
                pedido.setPrecoTotal(resultado.getDouble("precoTotal"));

                lista.add(pedido);
            }
            
            return lista;
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao listar pedidos no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public List<Pedido> listarPorTelefone(String telefone)
    {
        List<Pedido> lista = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listarPorTelefone);)
        {
            stmt.setString(1, telefone);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                while(resultado.next())
                {
                    Pedido pedido = new Pedido();
                    PizzaDAO dao = new PizzaDAO();
                    
                    pedido.setId(resultado.getInt("idPedido"));
                    pedido.setCliente(new Cliente(resultado.getInt("FK_idCliente"), resultado.getString("nome"), resultado.getString("sobrenome"), resultado.getString("telefone")));
                    pedido.setEstado(new EstadoPedido(resultado.getString("FK_estado")));
                    pedido.setPrecoTotal(resultado.getDouble("precoTotal"));
                    pedido.setPizzas(dao.listarPorPedido(resultado.getInt("idPedido")));
                    
                    lista.add(pedido);
                }
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao listar pedidos no banco de dados. Origem="+e.getMessage());
            }
            
            return lista;
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao listar pedidos no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public void alterarEstado(Pedido pedido)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.alterarEstado))
        {
            stmt.setString(1, pedido.getEstado().getEstado());
            stmt.setInt(2, pedido.getId());
            
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao alterar estado do pedido no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public void inserir(Pedido pedido)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.inserir, PreparedStatement.RETURN_GENERATED_KEYS);)
        {
            con.setAutoCommit(false);
            
            stmt.setInt(1, pedido.getCliente().getId());
            stmt.setString(2, pedido.getEstado().getEstado());
            stmt.setDouble(3, pedido.getPrecoTotal());
            
            stmt.executeUpdate();
            
            ResultSet chave = stmt.getGeneratedKeys();
            chave.next();
            
            pedido.setId(chave.getInt(1));
            con.commit();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao inserir pedido no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public void inserirPizza(int idPedido, int idPizza)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.inserirPizza))
        {
            stmt.setInt(1, idPedido);
            stmt.setInt(2, idPizza);

            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao inserir pizza no pedido no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public double buscarValorTotalPedido(int idPedido)
    {
        double valorTotal = 0;
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.buscarValorTotalPedido);)
        {
            stmt.setInt(1, idPedido);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                resultado.next();
                valorTotal = resultado.getDouble("precoTotal");
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Erro ao buscar o valor total do pedido no banco de dados. Origem="+e.getMessage());
            }
                
            return valorTotal;
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar o valor total do pedido no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public void atualizarValorTotalPedido(int idPedido, double valorTotal)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.atualizarValorTotalPedido);)
        {
            stmt.setDouble(1, valorTotal);
            stmt.setInt(2, idPedido);
            
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao atualizar o valor total do pedido no banco de dados. Origem="+e.getMessage());
        }
    }
}
