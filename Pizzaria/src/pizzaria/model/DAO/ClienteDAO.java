/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model.DAO;
import pizzaria.model.Cliente;

import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author slaif
 */
public class ClienteDAO 
{
    private final String listar = "SELECT * FROM Cliente";
    private final String inserir = "INSERT INTO Cliente(nome, sobrenome, telefone) VALUES (?, ?, ?)";
    private final String alterar = "UPDATE Cliente SET nome=?, sobrenome=?, telefone=? WHERE idCliente=?";
    private final String remover = "DELETE FROM Cliente WHERE idCliente=?";
    private final String buscarSobrenome = "SELECT * FROM Cliente WHERE sobrenome=?";
    private final String buscarTelefone = "SELECT * FROM Cliente WHERE telefone=?";
    
    public List<Cliente> listar()
    {
        List<Cliente> lista = new ArrayList<>();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.listar);
            ResultSet resultado = stmt.executeQuery();)
        {
            while(resultado.next())
            {
                Cliente cliente = new Cliente();
                
                cliente.setId(resultado.getInt("idCliente"));
                cliente.setNome(resultado.getString("nome"));
                cliente.setSobreNome(resultado.getString("sobrenome"));
                cliente.setTelefone(resultado.getString("telefone"));
                
                lista.add(cliente);
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar dados no banco de dados. Origem="+e.getMessage());
        }
        
        return lista;
    }
    
    public void inserir(Cliente cliente)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.inserir, PreparedStatement.RETURN_GENERATED_KEYS);)
        {
            stmt.setString(1, cliente.getNome());
            stmt.setString(2, cliente.getSobreNome());
            stmt.setString(3, cliente.getTelefone());
            
            stmt.executeUpdate();
            
            ResultSet chave = stmt.getGeneratedKeys();
            chave.next();
            
            cliente.setId(chave.getInt(1));
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao inserir cliente no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public void alterar(Cliente cliente)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.alterar);)
        {
            stmt.setString(1, cliente.getNome());
            stmt.setString(2, cliente.getSobreNome());
            stmt.setString(3, cliente.getTelefone());
            stmt.setInt(4, cliente.getId());
            
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao alterar cliente no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public void remover(Cliente cliente)
    {
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.remover))
        {
            stmt.setInt(1, cliente.getId());
            
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao remover cliente no banco de dados. Origem="+e.getMessage());
        }
    }
    
    public Cliente buscarSobrenome(String sobrenome)
    {
        Cliente cliente = new Cliente();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.buscarSobrenome);)
        {
            stmt.setString(1, sobrenome);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                if(resultado.next())
                {
                    cliente.setId(resultado.getInt("idCliente"));
                    cliente.setNome(resultado.getString("nome"));
                    cliente.setSobreNome(resultado.getString("sobrenome"));
                    cliente.setTelefone(resultado.getString("telefone"));
                }
                else
                {
                    throw new RuntimeException("Não existe cliente com esse nome!");
                }
            }
            catch(SQLException e)
            {
                throw new RuntimeException("Erro ao buscar nome no banco de dados. Origem="+e.getMessage());
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar nome no banco de dados. Origem="+e.getMessage());
        }
        
        return cliente;
    }
    
    public Cliente buscarTelefone(String telefone)
    {
        Cliente cliente = new Cliente();
        
        try(Connection con = ConnectionFactory.getConnection();
            PreparedStatement stmt = con.prepareStatement(this.buscarTelefone))
        {
            stmt.setString(1, telefone);
            
            try(ResultSet resultado = stmt.executeQuery();)
            {
                if(resultado.next())
                {
                    cliente.setId(resultado.getInt("idCliente"));
                    cliente.setNome(resultado.getString("nome"));
                    cliente.setSobreNome(resultado.getString("sobrenome"));
                    cliente.setTelefone(resultado.getString("telefone"));
                }
                else
                {
                    throw new RuntimeException("Não existe cliente com esse telefone!");
                }
            }
            catch(SQLException e)
            {
                throw new RuntimeException("Erro ao buscar telefone no banco de dados. Origem="+e.getMessage());
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException("Erro ao buscar telefone no banco de dados. Origem="+e.getMessage());
        }
        
        return cliente;
    }
}
