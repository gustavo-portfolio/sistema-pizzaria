/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

/**
 *
 * @author slaif
 */
public class EstadoPedido 
{
    private String estado;
    
    public EstadoPedido(){}
    
    public EstadoPedido(String estado)
    {
        this.estado = estado;
    }
    
    public String getEstado()
    {
        return this.estado;
    }
    
    public void setEstado(String estado)
    {
        this.estado = estado;
    }
}
