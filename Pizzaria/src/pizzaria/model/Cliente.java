/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

/**
 *
 * @author slaif
 */
public class Cliente 
{
    private int id;
    private String nome;
    private String sobreNome;
    private String telefone;
    
    public Cliente(){}
    
    public Cliente(String nome, String sobreNome, String telefone)
    {
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.telefone = telefone;
    }
    
    public Cliente(int id, String nome, String sobreNome, String telefone)
    {
        this.id = id;
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.telefone = telefone;
    }
    
    /* Getters */
    public int getId()
    {
        return this.id;
    }
    
    public String getNome()
    {
        return this.nome;
    }
    
    public String getSobreNome()
    {
        return this.sobreNome;
    }
    
    public String getTelefone()
    {
        return this.telefone;
    }
    
    /* Setters */
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setNome(String nome)
    {
        this.nome = nome;
    }
    
    public void setSobreNome(String sobreNome)
    {
        this.sobreNome = sobreNome;
    }
    
    public void setTelefone(String telefone)
    {
        this.telefone = telefone;
    }
}
