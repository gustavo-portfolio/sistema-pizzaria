/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

/**
 *
 * @author slaif
 */
public class Pizza 
{
    private int id;
    private double preco;
    private SaborPizza[] sabores = new SaborPizza[2];
    private Forma forma;
    
    public Pizza(){}
    
    /* Getters */
    public int getId()
    {
        return this.id;
    }
    
    public double getPreco()
    {
        this.preco = this.calcularPreco();
        return this.preco;
    }
    
    public double getArea()
    {
        return this.forma.calcularArea();
    }
    
    public SaborPizza[] getSabores()
    {
        return this.sabores;
    }
    
    public Forma getForma()
    {
        return this.forma;
    }
    
    /* Setters */
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setPreco(double preco)
    {
        this.preco = preco;
    }
    
    public void setForma(String forma)
    {
        if(forma.equalsIgnoreCase("Quadrado"))
            this.forma = new Quadrado();
        else if(forma.equalsIgnoreCase("Triangulo"))
            this.forma = new Triangulo();
        else
            this.forma = new Circulo();
    }
    
    public void setSabores(SaborPizza sabores[])
    {
        this.sabores = sabores;
    }
    
    /* Métodos */
    public void adicionarSabor(SaborPizza sabor)
    {
        for(int i = 0; i < 2; i++)
            if(this.sabores[i] == null)
            {
                this.sabores[i] = sabor;
                return;
            }
    }
    
    public double calcularPreco()
    {
        double metade1, metade2;
        
        metade1 = this.sabores[0].getTipo().getPrecoCM2() * this.forma.calcularArea();
        metade2 = this.sabores[1].getTipo().getPrecoCM2() * this.forma.calcularArea();
        
        return (metade1 + metade2) / 2;
    }
}
