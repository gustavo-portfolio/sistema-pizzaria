/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author slaif
 */
public class Pedido 
{
    private int idPedido;
    private double precoTotal;
    private Cliente cliente;
    private List<Pizza> pizzas = new ArrayList<>();
    private EstadoPedido estado;
    
    public Pedido()
    {
        this.cliente = new Cliente();
        this.estado = new EstadoPedido();
    }
    
    /* Getters */
    public int getId()
    {
        return this.idPedido;
    }
    
    public double getPrecoTotal()
    {
        //this.precoTotal = this.calcularPrecoTotal();
        return this.precoTotal;
    }
    
    public Cliente getCliente()
    {
        return this.cliente;
    }
    
    public List<Pizza> getPizzas()
    {
        return this.pizzas;
    }
    
    public EstadoPedido getEstado()
    {
        return this.estado;
    }
    
    /* Setters */
    public void setId(int id)
    {
        this.idPedido = id;
    }
    
    public void setPrecoTotal(double preco)
    {
        this.precoTotal = preco;
    }
    
    public void setCliente(Cliente cliente)
    {
        this.cliente = cliente;
    }
    
    public void setPizzas(List<Pizza> pizzas)
    {
        this.pizzas = pizzas;
    }
    
    public void setEstado(EstadoPedido estado)
    {
        this.estado = estado;
    }
    
    public void addPizza(Pizza pizza)
    {
        this.pizzas.add(pizza);
    }
    
    /* Métodos */
    /*
    public double calcularPrecoTotal() // Implementando
    {
        double total = 0;
        
        for(int i = 0; i < this.pizzas.size(); i++)
        {
            total += this.pizzas.get(i).getPreco();
        }
        
        return total;
    }
    
    public void alterarPizza(Pizza novaPizza, Pizza velhaPizza)
    {
        for(int i = 0; i < this.pizzas.size(); i++)
        {
            // Localizar a pizza para alterar
            if(this.pizzas.get(i).getSabores()[0].getSabor().equalsIgnoreCase(velhaPizza.getSabores()[0].getSabor())
                && this.pizzas.get(i).getSabores()[1].getSabor().equalsIgnoreCase(velhaPizza.getSabores()[1].getSabor())
                && this.pizzas.get(i).getPreco() == velhaPizza.getPreco()
                && this.pizzas.get(i).getForma().getNomeForma().equalsIgnoreCase(velhaPizza.getForma().getNomeForma()))
            {
                this.pizzas.set(i, novaPizza);
                break;
            }
        }
    }
    */
}
