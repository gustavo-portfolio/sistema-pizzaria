/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;
import java.lang.Math;

/**
 *
 * @author slaif
 */
public class Circulo extends Forma
{
    private double raio;
    private final String nomeForma = "Circulo";
    
    public Circulo(){}
    
    @Override
    public String getNomeForma()
    {
        return this.nomeForma;
    }
    
    @Override
    public boolean setTamanho(double tamanho)
    {
        if(tamanho >= 7 && tamanho <= 23)
        {
            this.raio = tamanho;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public double calcularArea()
    {
        return Math.PI * Math.pow(this.raio, 2);
    }
    
    @Override
    public double getLado()
    {
        return this.raio;
    }
}
