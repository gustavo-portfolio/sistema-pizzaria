/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pizzaria.model;

/**
 *
 * @author slaif
 */
public class SaborPizza 
{
    private String sabor;
    private TipoPizza tipo;
    
    public SaborPizza()
    {
        this.tipo = new TipoPizza();
    }
    
    public SaborPizza(String sabor, TipoPizza tipo)
    {
        this.sabor = sabor;
        this.tipo = tipo;
    }
    
    /* Getters */
    public String getSabor()
    {
        return this.sabor;
    }
    
    public TipoPizza getTipo()
    {
        return this.tipo;
    }
    
    /* Setters */
    public void setSabor(String sabor)
    {
        this.sabor = sabor;
    }
    
    public void setTipo(TipoPizza tipo)
    {
        this.tipo = tipo;
    }
}
