# Sistema Pizzaria

## Apresentação

Este sistema foi desenvolvido na disciplina de POO II. É possível visualizar os requisitos no pdf "enunciado_trabalho".

Versão 2.0 do sistema inclui refatoração do código para utilização dos padrões de projeto: MVC, DAO e Factory. A IDE utilizada foi o Apache NetBeans 17 com a JDK 19.0.1 . O SGBD utilizado foi o MySQL, para configurar o banco, basta usar o arquivo "MySQL.txt" e colocar o usuário e senha no arquivo "ConnectionFactory.java" (Pizzaria\src\pizzaria\model\DAO).

## Diagrama de Classes

「 Diagrama da versão 1.0 do software, a versão 2.0 utiliza o mesmo diagrama com a adição das variáveis "id" e transformação das classes enum para "classes normais" 」
![Diagrama de Classes](DiagramaClassesPizzaria.jpg)

## Diagrama Relacional

![Diagrama Relacional](DR.PNG)

## Códigos

O caminho para os arquivos se encontra em: Pizzaria\src\pizzaria

## Executando o projeto

Com o banco de dados devidamente configurado e, se necessário, ajuste da importação do mysql-connector, basta executar a classe Main.java no diretório "controller".

## Demonstração do sistema

Em breve, link do vídeo no Youtube.